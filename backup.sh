#!/bin/bash

# Copies the latest .savegame file contained in /astroneer/Saved/SaveGames to /astroneer/Saved/SaveGames/backup/bak$(date +%Y%m%d%H%M%S).savegame
# This is just a safety measure in case the savegame gets corrupted

# Set the path to the savegame folder
SAVEGAME_FOLDER="/astroneer/Astro/Saved/SaveGames"
BACKUP_FOLDER="/astroneer/Astro/Saved/SaveGames/backup"

# Ensure the backup folder exists
mkdir -p $BACKUP_FOLDER

# Get the latest savegame file
LATEST_SAVEGAME=$(ls -p $SAVEGAME_FOLDER | head -1)

# Copy the latest savegame file to the backup folder
cp $SAVEGAME_FOLDER/$LATEST_SAVEGAME $BACKUP_FOLDER/$LATEST_SAVEGAME.$(date +%Y-%m-%d_%H%M%S).bak

# Limit the contents of the backup folder to the latest 10 savegames
ls -t $BACKUP_FOLDER | tail -n +11 | xargs -I {} rm $BACKUP_FOLDER/{}